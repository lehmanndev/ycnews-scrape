#!/usr/bin/env python3

# Copyright (c) 2024 Thomas Lehmann
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of 
# this software and associated documentation files (the "Software"), to deal in 
# the Software without restriction, including without limitation the rights to 
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies 
# of the Software, and to permit persons to whom the Software is furnished to do 
# so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all 
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR 
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, 
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL 
# THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER 
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, 
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE 
# SOFTWARE.

# https://lxml.de/cssselect.html
from lxml.cssselect import CSSSelector
from lxml.etree import fromstring
from lxml.etree import HTMLParser
from lxml.etree import XML
from lxml.etree import tostring
from urllib import request
import json
import datetime
import re

BASE_URL_TO_PAGE="https://news.ycombinator.com/?p=%d"
PAGES=3
USER_AGENT="Mozilla/5.0 (X11; Linux x86_64; rv:123.0) Gecko/20100101 Firefox/123.0"

def extract_number(num_str):
	"""
	Extract numbers from 123., 123 comments, 123 points
	num_str: string - a string containing a number with prefix and or suffix
	return int of the number
	"""
	numbers_re = re.compile(r"^[^\d]*(\d+)[^\d]*$")
	return int(numbers_re.match(num_str).group(1))

def convert_time(iso_date_str):
	"""
	Take ISO date string and return seconds since epoch
	iso_date_str: string - "2024-02-27T18:29:48"
	returns int since 1970-01-01
	"""
	return int(datetime.datetime.fromisoformat(iso_date_str).timestamp())

def parse_page(page_markup):
	"""
	Parse page content HTML to extract items
	page_markup: string - markup
	returns dict of fields: id, rank, title, link, score, added, comments, collected
	"""
	
	collected = int(datetime.datetime.now().timestamp())

	root = fromstring(page_markup, HTMLParser())

	entry_sel = CSSSelector('.athing') # line with rank and title
	
	# child of entry
	rank_sel = CSSSelector('.rank') # [content] #.
	title_link_sel = CSSSelector('.titleline a') # [href], content
	# next sibling of the entry
	score_sel = CSSSelector('.subline .score') # content: # points
	added_sel = CSSSelector('.subtext .age') # [title]: 2024-02-28T15:39:31
	comments_sel = CSSSelector('a:last-child') # content: # comments

	entries = []

	for entry in entry_sel(root):
		post_id = int(entry.get("id"))

		rank = extract_number(rank_sel(entry)[0].text)
		title_link = title_link_sel(entry)[0]
		title = title_link.text
		link = title_link.get("href")

		subline = entry.getnext()

		score_els = score_sel(subline)
		score = 0
		if len(score_els) > 0:
			score = extract_number(score_sel(subline)[0].text)
		added = convert_time(added_sel(subline)[0].get("title"))
		comments_link = comments_sel(subline)[0]
		comments = extract_number(comments_link.text)

		entries.append({
			"id": post_id,
			"rank": rank,
			"title": title,
			"link": link,
			"score": score,
			"added": added,
			"comments": comments,
			"collected": collected,
		})

	return entries

def main():
	"""
	Scrape all entries on the first PAGES from YCombinator Hacker news.
	"""

	entries = []
	for page in range(1, PAGES + 1):
		with request.urlopen(request.Request(BASE_URL_TO_PAGE % (page), headers={ "User-Agent": USER_AGENT })) as f:
			page_markup = f.read().decode("utf-8")
			entries_per_page = parse_page(page_markup)
			entries.extend(entries_per_page)

	print(json.dumps(entries))

if __name__ == "__main__":
	main()
