# YCnews scraper

## Development

Build once:

```
$ make build-base
```

Develop:

```
$ ./run ./ycnews-scrape.py
```


## Usage

Build once:

```
$ make build
```

Usage:

```
$ ./ycnews-scrape
```
